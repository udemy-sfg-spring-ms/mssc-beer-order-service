package com.an1sht.beer.order.service.web.model;

public enum OrderStatusEnum {
    NEW, READY, PICKED_UP
}
