package com.an1sht.beer.order.service.services;

import com.an1sht.beer.order.service.domain.BeerOrder;
import com.an1sht.beer.order.service.domain.Customer;
import com.an1sht.beer.order.service.domain.OrderStatusEnum;
import com.an1sht.beer.order.service.repositories.BeerOrderRepository;
import com.an1sht.beer.order.service.repositories.CustomerRepository;
import com.an1sht.beer.order.service.web.mappers.BeerOrderMapper;
import com.an1sht.beer.order.service.web.model.BeerOrderDto;
import com.an1sht.beer.order.service.web.model.BeerOrderPagedList;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BeerOrderServiceImpl implements BeerOrderService {

    private final BeerOrderRepository beerOrderRepository;
    private final CustomerRepository customerRepository;
    private final BeerOrderMapper beerOrderMapper;
    private final ApplicationEventPublisher publisher;

    public BeerOrderServiceImpl(BeerOrderRepository beerOrderRepository, CustomerRepository customerRepository,
                                BeerOrderMapper beerOrderMapper, ApplicationEventPublisher publisher) {
        this.beerOrderRepository = beerOrderRepository;
        this.customerRepository = customerRepository;
        this.beerOrderMapper = beerOrderMapper;
        this.publisher = publisher;
    }

    @Override
    public BeerOrderPagedList listOrders(UUID customerId, Pageable pageable) {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (customerOptional.isPresent()) {
            Page<BeerOrder> beerOrders = beerOrderRepository.findAllByCustomer(customerOptional.get(), pageable);
            return new BeerOrderPagedList(beerOrders.stream().map(beerOrderMapper::beerOrderToDto).collect(Collectors.toList()),
                    PageRequest.of(
                            beerOrders.getPageable().getPageNumber(),
                            beerOrders.getPageable().getPageSize()),
                    beerOrders.getTotalElements());
        }
        return null;
    }

    @Override
    public BeerOrderDto placeOrder(UUID customerId, BeerOrderDto beerOrderDto) {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (customerOptional.isPresent()) {
            BeerOrder beerOrder = beerOrderMapper.dtoToBeerOrder(beerOrderDto);
            beerOrder.setId(null);
            beerOrder.setCustomer(customerOptional.get());
            beerOrder.setOrderStatus(OrderStatusEnum.NEW);
            beerOrder.getBeerOrderLines().forEach(line -> line.setBeerOrder(beerOrder));
            BeerOrder savedBeerOrder = beerOrderRepository.saveAndFlush(beerOrder);
            log.debug("Saved beer order: " + savedBeerOrder.getId());
            return beerOrderMapper.beerOrderToDto(savedBeerOrder);
        }
        throw new RuntimeException("Customer not found");
    }

    @Override
    public BeerOrderDto getOrderById(UUID customerId, UUID orderId) {
        return beerOrderMapper.beerOrderToDto(getOrder(customerId, orderId));
    }

    private BeerOrder getOrder(UUID customerId, UUID orderId) {
        Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if (customerOptional.isPresent()) {
            Optional<BeerOrder> beerOrderOptional = beerOrderRepository.findById(orderId);
            if (beerOrderOptional.isPresent()) {
                BeerOrder beerOrder = beerOrderOptional.get();
                if (beerOrder.getCustomer().getId().equals(customerId)) {
                    return beerOrder;
                }
            }
            throw new RuntimeException("Beer order not found");
        }
        throw new RuntimeException("Customer not found");

    }

    @Override
    public void pickupOrder(UUID customerId, UUID orderId) {
        BeerOrder beerOrder = getOrder(customerId, orderId);
        beerOrder.setOrderStatus(OrderStatusEnum.PICKED_UP);
        beerOrderRepository.save(beerOrder);
    }
}
