package com.an1sht.beer.order.service.domain;

public enum OrderStatusEnum {
    NEW, READY, PICKED_UP
}
